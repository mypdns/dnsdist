-- Local Addresses to bind to, in this case all ip/ip6 addresses
setLocal('0.0.0.0', { doTCP=true, reusePort=true }) -- Listen on ipv4, port 53
addLocal('[::]:53', { doTCP=true, reusePort=true }) -- listen on ipv6, port 53
