-- Cap and block DNS abusers

-- We have lowered the allowed QPS to 30 from 60 as it could seems like
-- There have been some abusive usage of our server :(

local dbr = dynBlockRulesGroup()
dbr:setQueryRate(20, 10, "Exceeded query rate", 86400, DNSAction.Drop)
dbr:setRCodeRate(DNSRCode.NXDOMAIN, 20, 10, "Exceeded NXD rate", 1800)
dbr:setRCodeRate(DNSRCode.SERVFAIL, 20, 10, "Exceeded ServFail rate", 1800)
dbr:setQTypeRate(DNSQType.ANY, 20, 10, "Exceeded ANY rate", 86400, DNSAction.Drop)
dbr:setResponseByteRate(10000, 10, "Exceeded resp BW rate", 1800)
-- do not add dynamic blocks for hosts in the 192.0.2.0/24 and 2001:db8::/32 ranges
dbr:excludeRange({"127.0.0.0/8", "::1",
        "1.2.3.4/32","fe80::fd3e:a8f2:1b96:85c3/64",
        "2.3.4.5/32","fe80::fd3e:a8f2:1b96:85c3/64",
        "3.4.5.6/32","fe80::fd3e:a8f2:1b96:85c3/64",
        "4.5.6.7/32","fe80::fd3e:a8f2:1b96:85c3/64",
        "5.6.7.8/32","fe80::fd3e:a8f2:1b96:85c3/64"})
-- except for 192.168.1.1
-- dbr:includeRange("192.168.1.1/32")

function maintenance()
  dbr:apply()
end