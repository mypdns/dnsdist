-- Get DNS records for opennic domains https://www.opennic.org/

newServer({address="5.132.191.104:53", name="ns1.vie.at.dns.opennic.glue", pool="OpenNic", order=30, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})
newServer({address="[2a03:3180:f:7::dfc6:cfb7]:53", name="ns1.vie.at.dns.opennic.glue", pool="OpenNic", order=30, weight=10, checkType="AAAA", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="[2a0d:2144::]:53", name="ns7.any.dns.opennic.glue", pool="OpenNic", order=30, weight=10, checkType="AAAA", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="207.148.83.241:53", pool="OpenNic", name="ns5.nsw.au.dns.opennic.glue", order=32, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})
newServer({address="[2001:19f0:5801:11:5400:ff:fe2d:7724]:53", pool="OpenNic", name="ns5.nsw.au.dns.opennic.glue", order=32, weight=10, checkType="AAAA", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="111.67.20.8:53", pool="OpenNic", name="ns1.vic.au.dns.opennic.glue", order=32, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="163.53.248.170:53", pool="OpenNic", name="ns2.vic.au.dns.opennic.glue", order=32, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="103.236.162.119:53", pool="OpenNic", name="ns3.vic.au.dns.opennic.glue", order=32, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="142.4.204.111:53", pool="OpenNic", name="ns3.ca.dns.opennic.glue", order=32, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})
newServer({address="[2607:5300:120:a8a:142:4:204:111]:53", name="ns3.ca.dns.opennic.glue", pool="OpenNic", order=32, weight=10, checkType="AAAA", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="142.4.205.47:53", pool="OpenNic", name="ns4.ca.dns.opennic.glue", order=32, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})
newServer({address="[2607:5300:120:a8a:142:4:205:47]:53", name="ns4.ca.dns.opennic.glue", pool="OpenNic", order=32, weight=10, checkType="AAAA", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="192.99.85.244:53", pool="OpenNic", name="ns3.qc.ca.dns.opennic.glue", order=32, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="31.171.251.118:53", pool="OpenNic", name="ns1.zh.ch.dns.opennic.glue", order=32, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="82.141.39.32:53", pool="OpenNic", name="ns1.de.dns.opennic.glue", order=32, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})
newServer({address="[2a02:248:2:31c0:5054:ff:fe80:88]:53", name="ns1.de.dns.opennic.glue", pool="OpenNic", order=32, weight=10, checkType="AAAA", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="50.3.82.215:53", pool="OpenNic", name="ns7.de.dns.opennic.glue", order=32, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

newServer({address="46.101.70.183:53", pool="OpenNic", name="ns9.de.dns.opennic.glue", order=32, weight=10, checkType="A", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})
newServer({address="[2a03:b0c0:3:d0::283:1001]:53", name="ns9.de.dns.opennic.glue", pool="OpenNic", order=32, weight=10, checkType="AAAA", checkName="www.beauty-teens.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

-- This isn't the complete list, and should be a script that can automatically update from https://www.opennic.org/

addAction({'bbs', 'chan', 'cyb', 'dyn', 'geek', 'gopher', 'indy', 'libre', 'neo', 'null', 'o', 'oss', 'oz', 'parody', 'pirate'}, PoolAction("OpenNic")) -- Send all queries for to the "OpenNic" pool
-- setServerPolicyLua("orderedwrandom", orderedwrandom)

setServerPolicy(leastOutstanding)
