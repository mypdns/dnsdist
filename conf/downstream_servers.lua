newServer({address="162.159.0.33:53", name="ns3.cloudflare.com.", pool="cloudflare", checkType="A", checkName="cdnjs.cloudflare.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true, order=100, qps=200})
newServer({address="162.159.7.226:53", name="ns3.cloudflare.com.", pool="cloudflare", checkType="A", checkName="cdnjs.cloudflare.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true, order=100, qps=200})
addAction("cloudflare.com.", PoolAction("cloudflare"))
setServerPolicy(wrandom)

newServer({address="216.239.32.10:53", name="ns1.google.com.", pool="gstatic.com", checkType="A", checkName="fonts.gstatic.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true, order=100, qps=200})
newServer({address="216.239.34.10:53", name="ns2.google.com.", pool="gstatic.com", checkType="A", checkName="fonts.googleapis.com.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true, order=100, qps=200})
addAction({'fonts.googleapis.com.', 'fonts.gstatic.com.'}, PoolAction("gstatic.com"))
setServerPolicy(wrandom)


newServer({address="127.0.0.1:5301", name="ns1 recursor", checkType="A", checkName="ns1.dns.matrix.rocks.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true, order=1, qps=100000, pool="matrix"})
newServer({address="95.216.209.53", name="ns0 dnsdist", checkType="AAAA", checkName="ns0.dns.matrix.rocks.", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true, order=2, qps=100000, pool="matrix"})
addAction("matrix.rocks.", PoolAction("matrix"))
setServerPolicy(wrandom)

addAction(AllRule(), PoolAction("matrix"))