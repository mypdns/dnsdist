-- Get local DNS records for own domains to PowerDNS Auth

newServer({address="9.9.9.9:53", name="Quad9 Primary", pool="WhiteList", order=30, weight=10, checkType="A", checkName="ns1.dns.matrix.rocks", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})
newServer({address="149.112.112.112:53", name="Quad9 Primary", pool="WhiteList", order=30, weight=10, checkType="A", checkName="ns1.dns.matrix.rocks", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})
newServer({address="[2620:fe::fe, 2620:fe::9]:53", name="Quad9 IPv6", pool="WhiteList", order=30, weight=10, checkType="AAAA", checkName="mypdns.org", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

addAction({'mydomain1.com',
		'mydomain2.com'},
	PoolAction("WhiteList")) -- Send all queries for to the "myDomain" pool

setServerPolicy(leastOutstanding)
