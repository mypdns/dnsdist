-- Get local DNS records for own domains to PowerDNS Auth

newServer({address="127.0.0.1:5300", name="Localhost AUTH", pool="myDomain", order=30, weight=10, checkType="A", checkName="ns2.dns.matrix.rocks", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})
newServer({address="[::1]:5300", name="Localhost AUTH", pool="myDomain", order=30, weight=10, checkType="AAAA", checkName="ns0.dns.matrix.rocks", mustResolve=true, tcpRecvTimeout=10, tcpSendTimeout=10, retries=5, useClientSubnet=true})

addAction({'mydomain1.com', 'mydomain2.com'}, PoolAction("myDomain")) -- Send all queries for to the "myDomain" pool

setServerPolicy(leastOutstanding)
